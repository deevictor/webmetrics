import re
from lib import Kafka, Connector
from main import settings_file, section
from webpaser import bootstrap_servers


def test_kafka():
    """
    Tests kafka functionality. Creates test_topic, sends a sample via Kafka
    and confirms that received message is identical to original.
    """
    sample = "test sample8"
    test_topic = 'test_topic'
    id = 'test'
    kafka = Kafka(bootstrap_servers, test_topic, id)
    kafka.create_topic()
    producer = kafka.get_producer()
    producer.send(test_topic, value=sample)
    consumer = kafka.get_consumer()
    for message in consumer:
        assert message.value == sample
        break
    consumer.close()

def test_connector():
    """
    Tests the connection to PostgreSQL and confirms the connection by getting
    the version of it.
    """
    connector = Connector(settings_file=settings_file, section=section)

    # execute a statement
    connector.cursor.execute('SELECT version()')

    # get the PostgreSQL database server version
    db_version = connector.cursor.fetchone()
    assert re.match(".*PostgreSQL .*", str(db_version))
