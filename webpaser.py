from lib import Connector, Kafka, Parser


regexp = 'Performant data pipelines in minutes'
URL = 'https://aiven.io/'
topic = 'metrics'
bootstrap_servers = ['localhost:9092']
client_id = 'metrics'


def feed_metrics(settings_file, section):
    """Feeds metrics from Kafka to Postgresql"""

    connector = Connector(settings_file=settings_file, section=section)
    connector.cursor.execute(open("init.sql", "r").read())

    kafka = Kafka(bootstrap_servers, topic, client_id)
    consumer = kafka.get_consumer()

    try:
        for message in consumer:
            status = message.value['status']
            latency = message.value['latency']
            pattern = message.value['pattern']
            statement = f"INSERT INTO metrics VALUES ({status}, {latency}, '{pattern}')"
            print(statement)
            connector.cursor.execute(statement)

    except KeyboardInterrupt:
        print('Feeding interrupted')
        connector.close_connection()


def start_parser(URL=URL, topic=topic, regexp=regexp):
    """Initiates metrics reading and sending via Kafka."""

    kafka = Kafka(bootstrap_servers, topic, client_id)
    kafka.create_topic()
    producer = kafka.get_producer()

    parser = Parser(URL, topic, producer, regexp)
    parser.send_metrics()
