CREATE TABLE IF NOT EXISTS metrics (
    status varchar(200) NOT NULL,
    latency decimal(10,6) NOT NULL,
    pattern varchar(400) NOT NULL
)