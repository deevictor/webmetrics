from webpaser import feed_metrics, start_parser
from multiprocessing import Process


settings_file = 'settings.ini'
section = 'aiven_postgresql'

if __name__ == '__main__':
    try:
        #parallel launch of the parsing and sending
        p1 = Process(target=start_parser)
        p1.start()
        #parallel launch of the consuming and saving to the DB.
        p2 = Process(target=feed_metrics(settings_file, section))
        p2.start()
        p1.join()
        p2.join()
        # feed_metrics(settings_file, section)
    except KeyboardInterrupt:
        print('Interrupted')

