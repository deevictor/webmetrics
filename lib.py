import re
from json import dumps, loads
from time import sleep

import requests
from kafka import KafkaProducer, KafkaConsumer, KafkaClient, KafkaAdminClient
from configparser import ConfigParser
import psycopg2
from kafka.admin import NewTopic


class Parser:
    """Parser for the specific url, sends data to the topic in Kafka"""

    def __init__(self, url, topic, producer, regexp=None):
        self.url = url
        self.topic = topic
        self.producer = producer
        self.regexp = regexp

    def get_metrics(self):
        """Returns data dictionary with metrics."""

        r = requests.get(self.url)
        data = {
            'status': r.status_code,
            'latency': r.elapsed.total_seconds(),
        }
        if self.regexp:
            m = re.search(self.regexp, r.text)
            if m:
                pattern = m.group(0)
            else:
                pattern = ''
            data.update({'pattern': pattern})
        return data

    def send_metrics(self):
        """Sends metrics to Kafka topic"""

        while True:
            try:
                data = self.get_metrics()
                self.producer.send(self.topic, value=data)
                sleep(5)
            except KeyboardInterrupt:
                print('Generation interrupted')


class Kafka:
    """
    Kafka class for getting producer and consumer for the same topic and
    bootstrap servers.
    """

    def __init__(self, bootstrap_servers, topic, id):
        self.bootstrap_servers = bootstrap_servers
        self.topic = topic
        self.group_id = id
        self.client_id = id

    def get_producer(self):
        producer = KafkaProducer(bootstrap_servers=self.bootstrap_servers,
                                 value_serializer=lambda x:
                                 dumps(x).encode('utf-8')
                                 )
        return producer

    def get_consumer(self):
        consumer = KafkaConsumer(self.topic,
                                 bootstrap_servers=self.bootstrap_servers,
                                 auto_offset_reset='latest',
                                 enable_auto_commit=True,
                                 group_id=self.group_id,
                                 value_deserializer=lambda x: loads(
                                     x.decode('utf-8'))
                                 )
        return consumer

    def create_topic(self, admin_client=None):
        """Creates topic if it does not exist."""
        client = KafkaClient(bootstrap_servers='localhost:9092')
        future = client.cluster.request_update()
        client.poll(future=future)
        metadata = client.cluster
        if self.topic not in metadata.topics():
            topic_list = []
            topic_list.append(
                NewTopic(name=self.topic, num_partitions=1,
                         replication_factor=1))
            admin_client = KafkaAdminClient(
                bootstrap_servers=self.bootstrap_servers,
                client_id=self.client_id
            )
            admin_client.create_topics(new_topics=topic_list,
                                       validate_only=False)
        pass


class Settings:
    """Settings class to extract certain section in dict format"""

    def __init__(self, filename):
        self.filename = filename

    def get_config(self, section):
        """
        method for getting parameters from a section from the config file.
        """
        # create a parser
        parser = ConfigParser()
        # read config file
        parser.read(self.filename)

        section_data = {}
        if parser.has_section(section):
            params = parser.items(section)
            for param in params:
                section_data[param[0]] = param[1]
        else:
            raise Exception('Section {0} not found in the {1} file'.format(
                section, self.filename)
            )

        return section_data


class Connector:
    """Connector to Postgresql DB"""

    def __init__(self, settings_file, section):
        settings = Settings(settings_file)
        self.params = settings.get_config(section)
        self.conn = None
        self.cursor = self.get_cursor()

    def get_cursor(self):
        """method for getting the cursor for DB connection."""
        try:
            # connect to the PostgreSQL server
            print('Connecting to the PostgreSQL database...')
            self.conn = psycopg2.connect(**self.params)
            self.conn.autocommit = True

            # create a cursor
            cur = self.conn.cursor()

            return cur

        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
            return None

    def close_connection(self):
        if self.conn is not None:
            self.conn.close()
            print('Database connection closed.')
